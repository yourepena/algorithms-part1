package quickUnionUF;

public class QuickUnionUF {
    private int[] id;
	
    public QuickUnionUF(int n) {
        id = new int[n];
        for (int i = 0; i < n; i++)
            id[i] = i;
    }
    
    private int root(int n) {
    	while( n != id[n]) n = id[n];
    	return n;
    }
    
    public boolean connected(int a, int b) {
    	return this.root(a) == this.root(b);
    }
    
    public void union(int p, int q) {
    	int root1 = root(p);
    	int root2 = root(q);
    	id[root1] = root2;
    }
    
    
    public static void main(String[] args) {
		
    	QuickUnionUF uf = new QuickUnionUF(10);
    	
    	
    	
	}


}
