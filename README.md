# 1.5 UNION-FIND

| algorithm   | initialize | union | find |
|-------------|------------|-------|------|
| quick-find  | N          | N     | 1    |
| quick-union | N          | N †   | N    |
| Weighting quick-union | N          |  lg N †   | lg N    |  
| weighted QU + path compression| N | lg* N | lg* N

### Quick-find defect.
- Union too expensive (N array accesses).
- Trees are flat, but too expensive to keep them flat.

### Quick-union defect.
- Trees can get tall.
- Find too expensive (could be N array accesses).

### Quick Union Weighting 
- Modify quick-union to avoid tall trees.
- Keep track of size of each tree (number of objects).
- Balance by linking root of smaller tree to root of larger tree.

### Quick Union with Path Compression 